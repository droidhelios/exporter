package com.exporter.csv;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.exporter.R;
import com.exporter.listener.Callback;
import com.exporter.util.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class ExportCsvTask extends AsyncTask<Void, Void, File> {

    private final Context context;
    private File directory;
    private String fileName;
    private List<List<String>> csvData;
    private final Callback<File> listener;
    private String errorMessage = "";

    public ExportCsvTask(Context context, File directory, String fileName, List<List<String>> csvData, Callback<File> listener) {
        this.context = context;
        this.fileName = getFileName(fileName, ExportCsv.EXTENSION);
        this.csvData = csvData;
        this.directory = directory;
        this.listener = listener;
    }

    private String getFileName(String fileName, String extension) {
        if (TextUtils.isEmpty(fileName)) {
            fileName = context.getString(R.string.app_name);
        }
        return FileUtils.getFileName(fileName, extension);
    }

    @Override
    protected File doInBackground(Void... voids) {
        try {
            return exportDataInCsvFile();
        } catch (IOException e) {
            e.printStackTrace();
            this.errorMessage = e.getMessage();
            return null;
        }
    }

    @Override
    protected void onPostExecute(File result) {
        super.onPostExecute(result);
        if (result != null) {
            listener.onSuccess(result);
        } else {
            listener.onFailure(new Exception("Error:101 " + errorMessage));
        }
    }


    private File exportDataInCsvFile() throws IOException {

        if (!directory.exists()) {
            boolean status = directory.mkdirs();
        }
        File csvFile = new File(directory, fileName);
        FileWriter writer = new FileWriter(csvFile);

        for (int i = 0; i < csvData.size(); i++) {
            List<String> rowList = csvData.get(i);
            for (int j = 0; j < rowList.size(); j++) {
                writer.append(rowList.get(j));
                writer.append(',');
            }
            if (i != csvData.size() - 1)
                writer.append('\n');
        }
        writer.flush();
        writer.close();
        return csvFile;
    }


}
