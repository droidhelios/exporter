package com.exporter.listener;

public interface Selector<T> {

    void onSelect(T result);

}
