package com.exporter;

import android.content.Context;

import com.exporter.csv.ExportCsv;
import com.exporter.excel.ExportExcel;
import com.exporter.text.ExportText;

public class Exporter {
    private final Context context;

    private Exporter(Context context) {
        this.context = context;
    }

    public static Exporter getInstance(Context context) {
        return new Exporter(context);
    }


    public ExportText TextBuilder() {
        return new ExportText(context);
    }


    public ExportExcel ExcelBuilder() {
        return new ExportExcel(context);
    }

    public ExportCsv CsvBuilder() {
        return new ExportCsv(context);
    }

}
