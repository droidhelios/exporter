package com.exporter.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Abhijit Rao Created on 2019/2/19.
 */
public class FileUtils {

//    public static Uri getUriFromFile(Context context, File file) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
//        } else {
//            return Uri.fromFile(file);
//        }
//    }

    public static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }

    public static String getPathFromFile(File file) {
        return file.getAbsolutePath();
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat("yyyy_MM_dd_HHmmss", Locale.getDefault()).format(new Date());
    }

    public static File getNewFile(Context context, String fileName) {
        return getNewFile(context, fileName, true);
    }

    public static File getNewFile(Context context, String fileName, boolean isPrivate) {
        if (isPrivate) {
            return new File(getPrivateDirectory(), getFileName(fileName));
        } else {
            return new File(getPublicPictureDirectory(context), getFileName(fileName));
        }
    }

    public static String getFileName(String name) {
        return getFileName(name, ".jpg");

    }

    public static String getFileName(String prefix, String suffix) {
        return prefix + "_" + getTimeStamp() + suffix;
    }

    public static File getPublicPictureDirectory(Context context) {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public static File getPrivateFolderDirectory(String folderName) {
        return new File(Environment.getExternalStorageDirectory(), folderName);
    }

    public static File getPrivateDirectory() {
        return Environment.getExternalStorageDirectory();
    }


}
