package com.exporter.text;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;

import com.exporter.R;
import com.exporter.listener.Callback;
import com.exporter.util.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ExportTextTask extends AsyncTask<Void, Void, File> {

    private final Context context;
    private File directory;
    private String fileName;
    private final StringBuilder fileBody;
    private final Callback<File> listener;
    private String errorMessage = "";

    public ExportTextTask(Context context, File directory, String fileName, StringBuilder fileBody, Callback<File> listener) {
        this.context = context;
        this.fileName = getFileName(fileName,ExportText.EXTENSION);
        this.fileBody = fileBody;
        this.directory = directory;
        this.listener = listener;
    }

    private String getFileName(String fileName, String extension) {
        if (TextUtils.isEmpty(fileName)) {
            fileName = context.getString(R.string.app_name);
        }
        return FileUtils.getFileName(fileName, extension);
    }

    @Override
    protected File doInBackground(Void... voids) {

        try {
            return saveTextFile(fileName, fileBody);
        } catch (IOException e) {
            e.printStackTrace();
            this.errorMessage = e.getMessage();
            return null;
        }
    }

    @Override
    protected void onPostExecute(File result) {
        super.onPostExecute(result);
        if (result != null) {
            listener.onSuccess(result);
        } else {
            listener.onFailure(new Exception("Error:101 " + errorMessage));
        }
    }


    private File saveTextFile(String sFileName, StringBuilder fileBody) throws IOException {
        if (!directory.exists()) {
            boolean status = directory.mkdirs();
        }
        File textFile = new File(directory, sFileName);
        FileWriter writer = new FileWriter(textFile);
        writer.append(fileBody);
        writer.flush();
        writer.close();
        return textFile;
    }


}
