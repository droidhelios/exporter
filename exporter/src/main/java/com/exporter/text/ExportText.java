package com.exporter.text;

import android.Manifest;
import android.content.Context;

import com.exporter.R;
import com.exporter.dialog.FileDirectoryDialog;
import com.exporter.exception.Exceptions;
import com.exporter.listener.Callback;
import com.exporter.listener.Selector;
import com.exporter.util.FileUtils;
import com.exporter.util.ShowMessage;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.List;

public class ExportText {

    public static final String EXTENSION = ".txt";
    private final Context context;
    private String fileName;
    private StringBuilder fileBody;
    private Callback<File> listener;
    private File directory;

    public ExportText(Context context) {
        this.context = context;
    }

    public ExportText setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public ExportText setFileBody(String fileBody) {
        this.fileBody = new StringBuilder(fileBody);
        return this;
    }

    public ExportText setDirectory(File directory) {
        this.directory = directory;
        return this;
    }

    public ExportText setFileBody(StringBuilder fileBody) {
        this.fileBody = fileBody;
        return this;
    }

    public ExportText setListener(Callback<File> callback) {
        this.listener = callback;
        return this;
    }

    public File getDirectory() {
        if (directory == null) {
            directory = FileUtils.getPrivateFolderDirectory(context.getString(R.string.app_name));
        }
        return directory;
    }

    public void export() {
        if (isValidFields())
            checkPermission(new Callback<String>() {

                @Override
                public void onSuccess(String result) {
                    new ExportTextTask(context, getDirectory(), fileName, fileBody, listener).execute();
                }

                @Override
                public void onFailure(Exception e) {
                    listener.onFailure(e);
                }

            });
    }

    private boolean isValidFields() {
        if (listener != null) {
            if (fileBody == null) {
                listener.onFailure(new Exception(Exceptions.ERROR_FILE_BODY_INITIALIZATION));
                return false;
            }
            return true;
        } else {
            ShowMessage.toast(context, Exceptions.ERROR_LISTENER_INITIALIZATION);
            return false;
        }
    }

    private void checkPermission(final Callback<String> callback) {
        TedPermission.with(context)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        callback.onSuccess("");
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        callback.onFailure(new Exception(Exceptions.ERROR_PERMISSION_DENIED));
                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }



    public void showListOfFiles(Selector<File> selector) {
        showListOfFiles(null, selector);
    }
    public void showListOfFiles(String dialogTitle, Selector<File> selector) {
        File rootDirectory = getDirectory();
        File[] files = rootDirectory.listFiles();
        FileDirectoryDialog dialog = FileDirectoryDialog.newInstance(context,dialogTitle,selector, files);
        dialog.show();
    }
}
